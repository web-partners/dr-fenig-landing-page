<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" CodeFile="FenigLandingContact.ascx.cs" Inherits="FenigLandingContact" %>
<asp:UpdatePanel runat="server">
<ContentTemplate>
<asp:MultiView id="MltContact" runat="server">
<asp:View runat="server">
	<div id="contact-inputs">
		<h5><span>Call Dr. David Fenig Today</span><span class="large">877-699-2699</span><span>or visit <a href="/">www.drfenig.com</a></span></h5>
		<h6>Or fill out the form below:</h6>
		<div class="form-control hidden">
			<asp:Label AssociatedControlId="txtFirstName" Text="First Name" runat="server" />
			<asp:TextBox ID="txtFirstName" runat="server" />
		</div>
		<div class="form-control">
			<asp:Label AssociatedControlId="txtName" Text="First and Last Name" runat="server" />
			<asp:TextBox ID="txtName" runat="server" />
			<asp:RequiredFieldValidator Display="dynamic" CssClass="error" ControlToValidate="txtName" runat="server" ErrorMessage="Please enter your name." />
		</div>
		<div class="form-control">
			<asp:Label AssociatedControlId="txtPhone" Text="Phone Number" runat="server" />
			<asp:TextBox ID="txtPhone" runat="server" type="tel" />
		</div>
		<div class="form-control">
			<asp:Label AssociatedControlId="txtEmail" Text="Email" runat="server" />
			<asp:TextBox ID="txtEmail" runat="server" type="email" />
			<asp:RegularExpressionValidator Display="dynamic" CssClass="error" ControlToValidate="txtEmail" ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ErrorMessage="Invalid e-mail." runat="server"/>
		</div>
		<div class="form-control">
			<asp:Label AssociatedControlId="txtMessage" Text="Comments" runat="server" />
			<asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" />
			<asp:RequiredFieldValidator Display="dynamic" CssClass="error" ControlToValidate="txtMessage" runat="server" ErrorMessage="Please enter a message" />
		</div>
	</div>
	<div id="contact-submit">
		<asp:Button ID="ContactSubmitButton" runat="server" OnClick="ContactSubmitClick" Text="Submit" />
	</div>
</asp:View>
<asp:View runat="server">
	<asp:Literal runat="server" id="litThankYou" />
</asp:View>
</asp:MultiView>
</ContentTemplate>
</asp:UpdatePanel>