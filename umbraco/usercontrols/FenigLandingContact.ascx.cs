using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Web.Configuration;


public partial class FenigLandingContact : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if(!Page.IsPostBack) {
			MltContact.ActiveViewIndex = 0;
		}
	}
	protected void ContactSubmitClick(object sender, EventArgs e)
	{
		Page.Validate();
		if (Page.IsValid)
		{
			MltContact.ActiveViewIndex = 1;
			if (String.IsNullOrEmpty(txtFirstName.Text.Trim()))
			{
				try
				{
					MailMessage mail = new MailMessage();
					SmtpClient SmtpServer = new SmtpClient();
					mail.From = new MailAddress(txtEmail.Text.Trim(),txtName.Text.Trim());
					mail.To.Add(new MailAddress("abarsotti@tivilon.com"));
					mail.Subject = "Dr. Fenig Landing Page Contact Form Submitted";
					mail.Body = "<strong>Name: </strong>" + txtName.Text.Trim();
					if(!String.IsNullOrEmpty(txtPhone.Text.Trim()))
					{
						mail.Body += "<br/><strong>Phone Number: </strong>" + txtPhone.Text.Trim();
					} 
					mail.Body += "<br/><strong>Message: </strong>" + txtMessage.Text.Trim();
					mail.IsBodyHtml = true;
					SmtpServer.EnableSsl = false;
					SmtpServer.Send(mail);
					litThankYou.Text = "<p>Thank you for contacting us!</p>";
				}
				catch (Exception ex)
				{
					litThankYou.Text = "<p>An error occurred. Please go back and try again.</p>";
				}
			}
			else
			{
				litThankYou.Text = "<p>An error occurred. Please go back and try again.</p>";
			}
		}
	}
}