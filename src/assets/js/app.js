(function($) {
	var headerResize = function() {
		if($(window).width() < 1000) {
			var bannerHeight = ($(window).width() / 960) * 295;
			$('#header').height(bannerHeight);
		}
		else {
			$('#header').height(295);
		}
	};
	headerResize();
	$(window).resize(headerResize);
	$('#contact-inputs').find('input, textarea').on('keyup, focus', function() {
		$('#contact-inputs').find('input, textarea').each(function() {
			if($(this).siblings('.error').is(':visible') && !$(this).closest('.form-control').hasClass('has-error')) {
				$(this).closest('.form-control').addClass('has-error');
			}
			else if(!$(this).siblings('.error').is(':visible') && $(this).closest('.form-control').hasClass('has-error')) {
				$(this).closest('.form-control').removeClass('has-error');
			}
		});
	});
	Modernizr.addTest('backgroundclip',function() {
		var div = document.createElement('div');
		if ('backgroundClip' in div.style) {
			return true;
		}
		'Webkit Moz O ms Khtml'.replace(/([A-Za-z]*)/g,function(val) { 
			if (val+'BackgroundClip' in div.style) {
				return true;
			}
		});
	});
})(jQuery);